using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInventory : MonoBehaviour
{
    [SerializeField] Transform WeaponLocation;
    [SerializeField] GameObject SampleWeapon_Prefab;

    [SerializeField] private GameObject[] Weapons;
    [SerializeField] private int currentWeaponIndex;

    [SerializeField] private int currentPistolAmmo = 5;
    [SerializeField] private int currentAK47Ammo = 30;
    [SerializeField] private int currentShotgunAmmo = 5;

    private bool isWeaponEquipped;
    void Start()
    {
        Weapons = new GameObject[2];
        AddWeapon(SampleWeapon_Prefab);
        currentPistolAmmo = 5;
        currentAK47Ammo = 30;
        currentShotgunAmmo = 5;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // FWeapon
    public WeaponBaseClass getWeaponIn(int index)
    {
        if (Weapons[index])
        {
            return Weapons[index].GetComponent<WeaponBaseClass>();
        }

        return null;
    }
    public int getCurrentWeaponIndex() { return currentWeaponIndex; }
    // Equipped Weapon
    public WeaponBaseClass getEquippedWeapon() { return Weapons[currentWeaponIndex].GetComponent<WeaponBaseClass>(); }
    public void setEquippedWeapon(int index) 
    {
        if (!Weapons[index])
        {
            Debug.Log("Weapon is null at " + index);
            return;
        }
        
        for (int i = 0; i < Weapons.Length; i++)
        {
            if (i == index)
            {
                currentWeaponIndex = i;
                Weapons[i].SetActive(true);
            }
            else
            {
                if (Weapons[i])
                {
                    Weapons[i].SetActive(false);
                }
            }
        }
    }

    public void AddWeapon(GameObject newWeapon)
    {
        if (newWeapon.GetComponent<WeaponBaseClass>())
        {
            for (int i = 0; i < Weapons.Length; i++)
            {
                if (Weapons[i] == null)
                {
                    Weapons[i] = Instantiate(newWeapon, WeaponLocation.position, WeaponLocation.rotation);
                    Weapons[i].transform.parent = this.transform;
                    setEquippedWeapon(i);
                    isWeaponEquipped = true;
                    return;
                }
            }
            Destroy(Weapons[currentWeaponIndex]);
            Weapons[currentWeaponIndex] = Instantiate(newWeapon, WeaponLocation.position, WeaponLocation.rotation);
            Weapons[currentWeaponIndex].transform.parent = this.transform;
            setEquippedWeapon(currentWeaponIndex);
        }
    }

    // Pistol Ammo
    public int getPistolAmmo() { return currentPistolAmmo; }
    public void minusPistolAmmo(int value) { currentPistolAmmo -= value; }
    public void addPistolAmmo(int value) { currentPistolAmmo += value; }
    public void setPistolAmmo(int value) { currentPistolAmmo = value; }

    // AK47 Ammo
    public int getAK47Ammo() { return currentAK47Ammo; }
    public void minusAK47Ammo(int value) { currentAK47Ammo -= value; }
    public void addAK47Ammo(int value) { currentAK47Ammo += value; }
    public void setAK47Ammo(int value) { currentAK47Ammo = value; }

    // Shotgun Ammo
    public int getShotgunAmmo() { return currentShotgunAmmo; }
    public void minusShotgunAmmo(int value) { currentShotgunAmmo -= value; }
    public void addShotgunAmmo(int value) { currentShotgunAmmo += value; }
    public void setShotgunAmmo(int value) { currentShotgunAmmo = value; }

    //bool
    public bool getIsWeaponEquipped() { return isWeaponEquipped; }
    
    //
    public void ReloadEquippedGun()
    {
        WeaponBaseClass weapon = getEquippedWeapon();
        if (weapon)
        {
            switch(weapon.getWeaponType())
            {
                case WeaponType.Pistol:
                    currentPistolAmmo = weapon.ReloadClip(currentPistolAmmo);
                    break;

                case WeaponType.AK47:
                    currentAK47Ammo = weapon.ReloadClip(currentAK47Ammo);
                    break;

                case WeaponType.Shotgun:
                    currentShotgunAmmo = weapon.ReloadClip(currentShotgunAmmo);
                    break;
            }
        }
    }

    public bool isWeaponSlotFull()
    {
        foreach (GameObject GObject in Weapons)
        {
            if (GObject == null)
            {
                return false;
            }
        }

        return true;
    }
}
