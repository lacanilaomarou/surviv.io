using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Gamepad.current.startButton.wasPressedThisFrame)
        {
            SceneManager.LoadScene(1);
        }
    }

    public void OnStartClick()
    {
        SceneManager.LoadScene(1);
    }
}
