using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    [SerializeField] Slider healthSlider;
    [SerializeField] Image Fill;
    [SerializeField] GameObject PlayerObj;
    [SerializeField] Text PistolTotalAmmo_TXT;
    [SerializeField] Text AK47TotalAmmo_TXT;
    [SerializeField] Text ShotgunTotalAmmo_TXT;
    [SerializeField] Text[] ClipAmmo_TXT;
    [SerializeField] Text[] Weapon_TXT;

    CharacterStats PlayerStats;
    CharacterInventory PlayerInventory;
    WeaponBaseClass PlayerWeaponClass;



    void Start()
    {
        PlayerStats = PlayerObj.GetComponent<CharacterStats>();
        PlayerInventory = PlayerObj.GetComponent<CharacterInventory>();
        PlayerWeaponClass = PlayerInventory.getEquippedWeapon();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateHPBar();
        UpdateAmmoText();
    }

    void UpdateHPBar()
    {
        healthSlider.value = PlayerStats.getHealthPercent();

        if(PlayerStats.getHealthPercent() >0.2f)
        {
            Fill.color = Color.green;
        }
        else
        {
            Fill.color = Color.red;
        }
    }

    void UpdateAmmoText()
    {
        // ClipAmmo_TXT.text = PlayerInventory.getEquippedWeapon().getCurrentClipAmmo() + "/" 
        //     + PlayerInventory.getEquippedWeapon().getMaxClipAmmo();

        for (int i = 0; i < Weapon_TXT.Length; i++)
        {
            if (PlayerInventory.getWeaponIn(i) == null)
            {
                Weapon_TXT[i].color = Color.grey;
                ClipAmmo_TXT[i].color = Color.grey;
                Weapon_TXT[i].text = "N/A";
                ClipAmmo_TXT[i].text = "0/0";

            }
            else
            {
                Weapon_TXT[i].text = PlayerInventory.getWeaponIn(i).getWeaponType().ToString();
                ClipAmmo_TXT[i].text = PlayerInventory.getWeaponIn(i).getCurrentClipAmmo() + "/"
                    + PlayerInventory.getEquippedWeapon().getMaxClipAmmo();

                if (PlayerInventory.getCurrentWeaponIndex() == i)
                {
                    Weapon_TXT[i].color = Color.green;
                    ClipAmmo_TXT[i].color = Color.green;
                }
                else
                {
                    Weapon_TXT[i].color = Color.grey;
                    ClipAmmo_TXT[i].color = Color.grey;
                }
            }
        }
        PistolTotalAmmo_TXT.text = "Pistol Ammo: " + PlayerInventory.getPistolAmmo();
        AK47TotalAmmo_TXT.text = "Ak47 Ammo: " + PlayerInventory.getAK47Ammo();
        ShotgunTotalAmmo_TXT.text = "Shotgun Ammo: " + PlayerInventory.getShotgunAmmo();
    }
}
