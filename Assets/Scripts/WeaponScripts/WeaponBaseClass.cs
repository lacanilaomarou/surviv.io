using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum WeaponType
{
    Pistol,
    AK47,
    Shotgun,
}

public class WeaponBaseClass : MonoBehaviour
{
    [SerializeField] protected Transform muzzlePoint;
    [SerializeField] protected GameObject bullet_Prefab;
    [SerializeField] protected float weaponDMG;
    [SerializeField] protected int maxClipAmmo;
    [SerializeField] protected float fireRate;
    [SerializeField] protected float bulletForce;
    [SerializeField] protected float range = 20.0f;

    protected WeaponType weaponType;
    protected int currentClipAmmo;
    protected bool isWaiting;
    protected bool isTriggerDown;
    protected virtual void Start()
    {
        currentClipAmmo = maxClipAmmo;
    }

    private void OnEnable()
    {
        isWaiting = false;
        isTriggerDown = false;
    }

    protected void InitBullet(Transform mTransform)
    {
        RaycastHit2D hit = Physics2D.Raycast(mTransform.position, mTransform.up, 50.0f);
        
  
        GameObject bullet = Instantiate(bullet_Prefab, mTransform.position, mTransform.rotation);

        BulletBehavior bb = bullet.GetComponent<BulletBehavior>();

        if (bb != null)
        {
            bb.setBulletSpeed(bulletForce);

            if (hit)
            {
                if (hit.transform.gameObject.GetComponent<CharacterStats>())
                {
                    CharacterStats stats = hit.transform.gameObject.GetComponent<CharacterStats>();
                    stats.damageHealth(weaponDMG);
                }
            }
        }
        else Debug.LogError("Bullet Class is NULL!");
    }

    public virtual IEnumerator OnShoot()
    {
        yield return new WaitForSeconds(0);
    }

    public void setTriggerPress(bool value) { isTriggerDown = value; }
    public int ReloadClip(int totalAmmo)
    {
        int difference = maxClipAmmo - currentClipAmmo;
        if(totalAmmo > difference)
        {
            currentClipAmmo += difference;
            return totalAmmo - difference;
        }
        else
        {
            currentClipAmmo += totalAmmo;
            return 0;
        }
    }

    public bool isTimeToReload()
    {
        return currentClipAmmo <= 0;
    }

    public int getCurrentClipAmmo() { return currentClipAmmo; }
    public int getMaxClipAmmo() { return maxClipAmmo; }
    public WeaponType getWeaponType() { return weaponType; }
    
    public float getFireRate() { return fireRate; }
    
}
