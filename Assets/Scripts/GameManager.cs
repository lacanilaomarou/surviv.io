using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    static public GameObject[] PatrolPoints;

    [SerializeField]  GameObject UIPanel;

    [SerializeField] private GameObject ControlsPanel;

    [SerializeField] private GameObject GameOverScreenPanel;
    
    [SerializeField] private Text Result_TXT;

    private List<GameObject> EnemiesAlive;

    private bool isGameOver;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        EnemiesAlive = new List<GameObject>();
        PatrolPoints = GameObject.FindGameObjectsWithTag("PatrolSpots");
        EnemiesAlive = GameObject.FindGameObjectsWithTag("Enemy").ToList();
        isGameOver = false;

        StartCoroutine(CheckIfGameIsFinish());
    }

    // Update is called once per frame
    void Update()
    {
        if (isGameOver)
        {
            if (Gamepad.current.startButton.wasPressedThisFrame)
            {
                SceneManager.LoadScene(1);
            }

            if (Gamepad.current.selectButton.wasPressedThisFrame)
            {
                SceneManager.LoadScene(0);
            }
        }
    }

    public void OnGameOver(bool PlayerWon)
    {
        Time.timeScale = 0;
        UIPanel.SetActive(false);
        ControlsPanel.SetActive(false);
        GameOverScreenPanel.SetActive(true);
        isGameOver = true;

        if (PlayerWon)
        {
            Result_TXT.text = "You Win!";
            Result_TXT.color = Color.green;
        }

        else
        {
            Result_TXT.text = "You Lose!";
            Result_TXT.color = Color.red;
        }
    }

    IEnumerator CheckIfGameIsFinish()
    {
        while (!isGameOver)
        {
            EnemiesAlive.Clear();
            EnemiesAlive = GameObject.FindGameObjectsWithTag("Enemy").ToList();
            
            if (EnemiesAlive.Count <= 0)
            {
                OnGameOver(true);
            }
            yield return new WaitForSeconds(1.0f);
        }
    }
}
