using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class ShootEnemyNearby : StateMachineBehaviour
{
    private EnemyScript enemy;
    private IAstarAI AI;

    private GameObject target;

    private float timeBtwShots;
    private float startBtwShots;

    private float stoppingDistance = 3.0f;

    private float retreatDistance = 5.0f;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log("Enemy Nearby");
        
        enemy = animator.GetComponent<EnemyScript>();
        AI = enemy.getAI();
        
        startBtwShots = Random.Range(1.0f, 2.0f);
        timeBtwShots = startBtwShots;

        AI.maxSpeed = 1;
        foreach (GameObject GO in enemy.OverlappedObjects)
        {
            if (GO.CompareTag("Player") || GO.CompareTag("Enemy"))
            {
                if (GO != null)
                {
                    target = GO;
                    AI.isStopped = true;
                    Debug.Log(target.name);
                }
            }
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (target != null)
        {
            //Debug.Log(Vector2.Distance(animator.transform.position,target.transform.position));
            Vector3 targetloc = target.transform.position;
            Vector2 direction = targetloc - animator.transform.position;
            animator.transform.up = direction;
            
            // go near Target
            // if(Vector2.Distance(animator.transform.position, target.transform.position) > stoppingDistance)
            // {
            //     AI.destination = target.transform.position;
            // }
            // else if (Vector2.Distance(animator.transform.position, target.transform.position) < stoppingDistance &&
            //          Vector2.Distance(animator.transform.position, target.transform.position) > retreatDistance)
            // {
            //     AI.destination = animator.transform.position;
            // }
            // else if (Vector2.Distance(animator.transform.position, target.transform.position) > retreatDistance)
            // {
            //     AI.destination = Vector2.down*(retreatDistance);
            // }
            // else if (Vector2.Distance(animator.transform.position, target.transform.position) > 15.0f)
            // {
            //     animator.SetBool("isEnemyNearby", false);
            //     animator.SetBool("IsPatrolling", true);
            // }
            
            
            
            
            // reload if neeeded
            if (enemy.getInventory().getEquippedWeapon().isTimeToReload())
            {
                enemy.getInventory().ReloadEquippedGun();
            }
            // shoot enemy
            if (timeBtwShots <= 0)
            {
                if (enemy.getInventory().getIsWeaponEquipped())
                {
                    enemy.OnShootGun();
                }

                timeBtwShots = startBtwShots;
            }
            else timeBtwShots -= Time.deltaTime;
        }
        else if (target == null)
        {
            animator.SetBool("isEnemyNearby", false);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Implement code that processes and affects root motion
    }

    // OnStateIK is called right after Animator.OnAnimatorIK()
    override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Implement code that sets up animation IK (inverse kinematics)
    }
}
