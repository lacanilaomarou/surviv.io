using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupAmmo : PickupBaseScript
{
    private enum AmmoType
    {
        Pistol,
        Ak47,
        Shotgun,
    }

    [SerializeField] private AmmoType ammoType;

    [SerializeField] private int ammoAmount;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnInteract(GameObject player)
    {

        switch (ammoType)
        {
            case AmmoType.Pistol:
                player.GetComponent<CharacterInventory>().addPistolAmmo(ammoAmount);
                break;
            case AmmoType.Ak47:
                player.GetComponent<CharacterInventory>().addAK47Ammo(ammoAmount);
                break;
            case AmmoType.Shotgun:
                player.GetComponent<CharacterInventory>().addShotgunAmmo(ammoAmount);
                break;
        }
        Destroy(this.gameObject);
        base.OnInteract(player);
    }
}
