using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class SpawnPickup : MonoBehaviour
{
    [SerializeField] private GameObject MapFloor;
    [SerializeField] private int MaxNumberToSpawn = 50;
    [SerializeField] private List<GameObject> PickupPrefabs;
    [SerializeField] private GameObject EnemyPrefab;

    private Vector2 minVector;
    private Vector2 maxVector;

    private List<GameObject> Spawned_Pickups;
    private GameObject[] Obstacles;
    
    void Start()
    {
        Spawned_Pickups = new List<GameObject>();
        Spawned_Pickups.AddRange(GameObject.FindGameObjectsWithTag("Pickup"));
        Debug.Log(Spawned_Pickups.Count);
        
        Obstacles = GameObject.FindGameObjectsWithTag("Obstacle");

        minVector = MapFloor.GetComponent<SpriteRenderer>().bounds.min;
        maxVector = MapFloor.GetComponent<SpriteRenderer>().bounds.max;

        RandomSpawnPickup();
        SpawnEnemies();
        // Debug.Log(minVector);
        // Debug.Log(maxVector);
        // Debug.Log(Obstacles.Length);
        Debug.Log(Spawned_Pickups.Count);
    }
    
    void Update()
    {
        
    }

    void RandomSpawnPickup()
    {
        for (int i = 0; i < MaxNumberToSpawn; i++)
        {
            int rndPickup = Random.Range(0, PickupPrefabs.Count);
            float rndX = Random.Range(minVector.x, maxVector.x);
            float rndY = Random.Range(minVector.y, maxVector.y);
            Vector2 rndVector = new Vector2(rndX, rndY);
            GameObject pickup = Instantiate(PickupPrefabs[rndPickup], rndVector, this.transform.rotation);

            if (hasCollsion(pickup))
            {
                Destroy(pickup);
            }
            else Spawned_Pickups.Add(pickup);
        }
    }

    void SpawnEnemies()
    {
        for (int i = 0; i < 20; i++)
        {
            float rndX = Random.Range(minVector.x, maxVector.x);
            float rndY = Random.Range(minVector.y, maxVector.y);
            Vector2 rndVector = new Vector2(rndX, rndY);
            GameObject enemy = Instantiate(EnemyPrefab, rndVector, this.transform.rotation);

            if (enemyHasCollisonOnSpawn(enemy))
            {
                Destroy(enemy);
            }
        }
    }
    bool hasCollsion(GameObject obj)
    {
        for (int i = 0; i < Obstacles.Length; i++)
        {
            if (obj.GetComponent<BoxCollider2D>().bounds.Intersects(Obstacles[i].GetComponent<BoxCollider2D>().bounds))
            {
                return true;
            }
        }

        for (int j = 0; j < Spawned_Pickups.Count; j++)
        {
            if (obj.GetComponent<BoxCollider2D>().bounds
                .Intersects(Spawned_Pickups[j].GetComponent<BoxCollider2D>().bounds))
            {
                return true;
            }
        }
        return false;
    }

    bool enemyHasCollisonOnSpawn(GameObject obj)
    {
        for (int i = 0; i < Obstacles.Length; i++)
        {
            if (obj.GetComponent<CircleCollider2D>().bounds.Intersects(Obstacles[i].GetComponent<BoxCollider2D>().bounds))
            {
                return true;
            }
        }
        return false;
    }
}
