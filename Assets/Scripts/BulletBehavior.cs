using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehavior : MonoBehaviour
{
    [SerializeField] private Rigidbody2D rb;

    private float bulletSpeed = 0;
    void Start()
    {
        //Physics2D.IgnoreLayerCollision(4,4);
        
        StartCoroutine(SelfDestruct());
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.Translate(Vector2.up * bulletSpeed * Time.deltaTime);
        
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.gameObject.layer != 2)
        {
            Destroy(this.transform.gameObject);
        }
    }
    
    public void setBulletSpeed(float speed) { bulletSpeed = speed; }

    IEnumerator SelfDestruct()
    {
        yield return new WaitForSeconds(2f);
        Destroy(this.gameObject);
    }
}
