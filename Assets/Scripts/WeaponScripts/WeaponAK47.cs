using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponAK47 : WeaponBaseClass
{
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        weaponType = WeaponType.AK47;
    }

    public override IEnumerator OnShoot()
    {
        while (isTriggerDown)
        {
            if (!isWaiting)
            {
                if (currentClipAmmo > 0)
                {
                    isWaiting = true;
                    InitBullet(muzzlePoint);
                    currentClipAmmo--;
                    yield return new WaitForSeconds(fireRate);
                    isWaiting = false;
                }
            }
            if (!isTriggerDown) break;
        }
    }
}
