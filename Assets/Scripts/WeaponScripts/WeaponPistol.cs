using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPistol : WeaponBaseClass
{
    protected override void Start()
    {
        base.Start();
        weaponType = WeaponType.Pistol;
    }

    public override IEnumerator OnShoot()
    {
        if (!isWaiting)
        {
            if (currentClipAmmo > 0)
            {
                isWaiting = true;
                InitBullet(muzzlePoint);
                currentClipAmmo--;
                yield return new WaitForSeconds(fireRate);
                isWaiting = false;
            }
        }
    }
}
