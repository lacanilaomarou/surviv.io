using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using Pathfinding;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EnemyScript : MonoBehaviour
{
    [SerializeField] private Animator m_Animator;
    [SerializeField] private CharacterStats Stats;
    [SerializeField] private CharacterInventory Inventory;
    [SerializeField] TextMeshProUGUI HP_Text;
    [SerializeField] private List<GameObject> overlappedObjects;
    private IAstarAI AI;

    // Start is called before the first frame update
    void Start()
    {
        AI = this.gameObject.GetComponent<IAstarAI>();
        AI.maxSpeed = Stats.getMoveSpeed();
        
        Inventory.setPistolAmmo(50);
        Inventory.setAK47Ammo(120);
        Inventory.setShotgunAmmo(20);
        
        overlappedObjects = new List<GameObject>();
        m_Animator.SetBool("isPatrolling", true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            if (!Inventory.isWeaponSlotFull())
            {
                overlappedObjects.Add(other.gameObject);
                m_Animator.SetBool("isLooting", true);
            }
        }

        if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Enemy"))
        {
            //Debug.Log(other.transform.parent.name);
            overlappedObjects.Add(other.gameObject);
            m_Animator.SetBool("isEnemyNearby", true);
        }
    }
    
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            overlappedObjects.Remove(other.gameObject);
            //m_Animator.SetBool("isLooting", false);
        }

        if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Enemy"))
        {
            overlappedObjects.Remove(other.gameObject);
            m_Animator.SetBool("isEnemyNearby", false);
        }
    }

    public void OnShootGun()
    {
        StartCoroutine(shootGun_Implementation(Inventory.getEquippedWeapon().getFireRate()));
    }
    
    private IEnumerator shootGun_Implementation(float firerate)
    {
        Inventory.getEquippedWeapon().setTriggerPress(true);
        Inventory.getEquippedWeapon().StartCoroutine(Inventory.getEquippedWeapon().OnShoot());

        yield return new WaitForSeconds(firerate*2);
        Inventory.getEquippedWeapon().setTriggerPress(false);

        yield return new WaitForSeconds(0.5f);

    }
    
    public IAstarAI getAI() { return AI; }
    public CharacterInventory getInventory() { return Inventory; }
    
    public List<GameObject> OverlappedObjects
    {
        get => overlappedObjects;
        set => overlappedObjects = value;
    }
}
