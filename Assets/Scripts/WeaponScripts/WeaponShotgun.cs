using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponShotgun : WeaponBaseClass
{
    [SerializeField] private Transform muzzle2;
    [SerializeField] private Transform muzzle3;
    protected override void Start()
    {
        base.Start();
        weaponType = WeaponType.Shotgun;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override IEnumerator OnShoot()
    {
        if (!isWaiting)
        {
            if (currentClipAmmo > 0)
            {
                isWaiting = true;
                InitBullet(muzzlePoint);
                InitBullet(muzzle2);
                InitBullet(muzzle3);
                currentClipAmmo--;
                yield return new WaitForSeconds(fireRate);
                isWaiting = false;
            }
        }
    }
    
    
}
