using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;

public class LootBehavior : StateMachineBehaviour
{
    private EnemyScript enemy;
    private IAstarAI AI;
    private float timer;

    private PickupBaseScript pickup;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log("isLooting");
        enemy = animator.GetComponent<EnemyScript>();
        AI = animator.GetComponent<IAstarAI>();
        AI.maxSpeed = 3;
        AI.isStopped = false;
        timer = 5;
        foreach (GameObject GO in enemy.OverlappedObjects)
        {
            if (GO.CompareTag("Pickup"))
            {
                if (GO != null)
                {
                    pickup = GO.GetComponent<PickupBaseScript>();
                    AI.destination = pickup.transform.position;
                    Debug.Log(pickup.transform.position);
                    break;
                }
            }
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (pickup != null)
        {
            if (Vector2.Distance(animator.transform.position, pickup.transform.position) <= 1f)
            {
                pickup.OnInteract(animator.transform.gameObject);
                animator.SetBool("isLooting", false);
                Debug.Log("isLooting Done");
            }
        }
        else
        {
            Debug.Log("pickNULL");
            animator.SetBool("isPatrolling", true);
            animator.SetBool("isLooting", false);
        }

        if (timer <= 0)
        {
            animator.SetBool("isPatrolling", true);
            animator.SetBool("isLooting", false);
        }
        else
        {
            timer-= Time.deltaTime;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
