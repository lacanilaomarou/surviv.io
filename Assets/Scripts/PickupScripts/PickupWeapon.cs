using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupWeapon : PickupBaseScript
{
    [SerializeField] GameObject Weapon_Prefab;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnInteract(GameObject player)
    {
        player.GetComponent<CharacterInventory>().AddWeapon(Weapon_Prefab);
        this.gameObject.SetActive(false);
        base.OnInteract(player);
    }
}
