using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    [SerializeField] float MoveSpeed;
    [SerializeField] float MaxHealth;
    [SerializeField] private float CurentHealth;
    // Start is called before the first frame update
    void Start()
    {
        CurentHealth = MaxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnDeath()
    {
        if (this.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
            Debug.LogError("Enemy SHould DIE");
        }
        else if (this.gameObject.CompareTag("Player"))
        {
            this.gameObject.GetComponent<PlayerScript>().OnPlayerDeath();
        }
    }

    public float getMoveSpeed() { return MoveSpeed; }
    public void setMoveSpeed(float value) { MoveSpeed = value; }
    public float getMaxHealth() { return MaxHealth; }
    public float getCurrentHealth() { return CurentHealth; }
    public float getHealthPercent() { return CurentHealth / MaxHealth; }
    public void damageHealth(float damage)
    {
        CurentHealth = CurentHealth - damage;

        if (CurentHealth <= 0) OnDeath();
    }

    public void HealHealth(float healValue) { CurentHealth = Mathf.Clamp(CurentHealth + healValue, 0f, MaxHealth); }
}
