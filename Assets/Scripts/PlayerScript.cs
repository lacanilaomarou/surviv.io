using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerScript : MonoBehaviour
{
    [SerializeField] CharacterStats mStats;
    [SerializeField] Rigidbody2D rb;
    [SerializeField] Camera cam;
    [SerializeField] CharacterInventory PlayerInventory;
    [SerializeField] private GameObject InteractiveButton;
    [SerializeField] private GameManager m_GameManager;
    private PlayerControls PlayerInput;
    private Vector2 MovementStick;
    private Vector2 LookStick;

    private bool InteractIsPressed;
    private void Awake() 
    { 
        PlayerInput = new PlayerControls();
    }

    private void OnEnable() { PlayerInput.Enable(); }
    private void OnDisable() { PlayerInput.Disable(); }

    private void Start()
    {
        
    }

    void Update()
    {
        MovementStick = PlayerInput.Player.Move.ReadValue<Vector2>();
        LookStick = PlayerInput.Player.Look.ReadValue<Vector2>();

        // Use Weapon;
        if(Gamepad.current.rightTrigger.wasPressedThisFrame)
        {
            PlayerInventory.getEquippedWeapon().setTriggerPress(true);
            PlayerInventory.getEquippedWeapon().StartCoroutine(PlayerInventory.getEquippedWeapon().OnShoot());
        }
        if (Gamepad.current.rightTrigger.wasReleasedThisFrame)
        {
            PlayerInventory.getEquippedWeapon().setTriggerPress(false);
        }
        
        // Reload Gun
        if(Gamepad.current.buttonWest.wasPressedThisFrame)
        {
            PlayerInventory.ReloadEquippedGun();
        }

        // Interacting
        if (Gamepad.current.buttonSouth.wasPressedThisFrame)
        {
            InteractIsPressed = true;
        }
        if (Gamepad.current.buttonSouth.wasReleasedThisFrame)
        {
            InteractIsPressed = false;
        }

        if (Gamepad.current.buttonNorth.wasPressedThisFrame)
        {
            SwitchWeapon();
        }
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + MovementStick * mStats.getMoveSpeed() * Time.fixedDeltaTime);

        if (LookStick != Vector2.zero)
        {
            float angle = Mathf.Atan2(LookStick.y, LookStick.x) * Mathf.Rad2Deg - 90f;
            rb.rotation = angle;
        }
    }
    

   public bool getIsInteractPressed()
   {
       return InteractIsPressed; 
   }

   public void SwitchWeapon()
   {
       int index = PlayerInventory.getCurrentWeaponIndex() + 1;
       if (index >= 2) index = 0;
       
       PlayerInventory.setEquippedWeapon(index);
   }

   public void OnPlayerDeath()
   {
       Debug.Log("Player Dies");
       m_GameManager.OnGameOver(false);
   }
}
