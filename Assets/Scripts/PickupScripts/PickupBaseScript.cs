using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PickupBaseScript : MonoBehaviour
{
    // Start is called before the first frame update
    //[SerializeField] public Button InteractButton;
    
    void Start()
    {
        //InteractButton.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (other.gameObject.GetComponent<PlayerScript>().getIsInteractPressed())
            {
                OnInteract(other.gameObject);
            }
        }
    }

    public virtual void OnInteract(GameObject player)
    {
        Destroy(this.transform.gameObject);
    }
}
