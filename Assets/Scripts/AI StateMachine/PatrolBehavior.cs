using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Pathfinding;

public class PatrolBehavior : StateMachineBehaviour
{
    
    private IAstarAI AI;
    private float speed;
    private int randIndex;
    
    
    
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log("Patrolling");
        AI = animator.GetComponent<IAstarAI>();
        AI.maxSpeed = animator.gameObject.GetComponent<CharacterStats>().getMoveSpeed();
        AI.isStopped = false;
        randIndex = Random.Range(0, GameManager.PatrolPoints.Length);

        AI.destination = GameManager.PatrolPoints[randIndex].transform.position;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (AI.reachedDestination)
        {
            randIndex = Random.Range(0, GameManager.PatrolPoints.Length);

            AI.destination = GameManager.PatrolPoints[randIndex].transform.position;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //AI.destination = animator.transform.position;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
